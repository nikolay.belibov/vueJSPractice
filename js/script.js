document.addEventListener('DOMContentLoaded', function() {
    // addJavascript('map1.js','body');
    map1();
    chart();
});

function map1() {
    console.log(localStorage.apiKey);
    if (!localStorage.apiKey) {
        let apiKey = prompt("AIzaSyAiVYxSM_cueXP2PFReGt7kkI9g9hZfEUQ");
        localStorage.apiKey = apiKey;
    }
    console.log(localStorage.mapTypeId);
    if (!localStorage.mapTypeId) {
        let mapTypeId = 'terrain';
        localStorage.mapTypeId = mapTypeId;
    }

    Vue.use(VueGoogleMaps, {
        load: {
            key: localStorage['apiKey']
        }
    });

    // document.addEventListener('DOMContentLoaded', function() {
    Vue.component('google-map', VueGoogleMaps.Map);
    Vue.component('google-marker', VueGoogleMaps.Marker);
    Vue.component('ground-overlay', Vue.extend({
        render() {
            return '';
        },
        mixins: [VueGoogleMaps.MapElementMixin],
        props: ['source', 'bounds', 'opacity'],
        created() {},
        deferredReady: function() {
            this.$overlay = new google.maps.GroundOverlay(
                this.source,
                this.bounds
            );
            this.$overlay.setOpacity(this.opacity);
            this.$overlay.setMap(this.$map);
        },
        destroyed: function() {
            this.$overlay.setMap(null);
        },
    }));

    Vue.component('google-polygon', VueGoogleMaps.Polygon);
    Vue.component('google-polyline', VueGoogleMaps.Polyline);

    new Vue({
        el: '#vue-gMap',
        data: {
            // GmapMap
            gmapOptions:{
                center: {
//                    lat: 55.78691482543945,
//                    lng: 37.5087776184082

                    lat: 55.57930374145508,
                    lng: 38.23979568481445
//                    lat: 1.380,
//                    lng: 103.800
                },
                zoom: 16,
                tilt: 45,
                mapTypeId: localStorage['mapTypeId'],
            },
            // GmapMarker
            gmarkerOptions:{
                markers: [{
                    position: {
                        lat: 55.78691482543945,
                        lng: 37.5077776184082
                    }
                }, {
                    position: {
                        lat: 55.57930374145508,
                        lng: 38.23979568481445
                    }
                }]
            },
            // GroundOverlay
            groundOverlayOptions:{
                bounds: {
                    north: 1.502,
                    south: 1.185,
                    east: 104.0262,
                    west: 103.5998,
                },
                opacity: 0.5,
            },
            // GmapPolygon
            gmapPolygonOptions:{
                editable: true,
                paths: [
                    [
                        {lat: 55.78691482543945, lng: 37.5087776184082},
                        {lat: 55.78682327270508, lng: 37.50876998901367},
                        {lat: 55.786827087402344, lng: 37.508785247802734},
                        {lat: 55.78738784790039, lng: 37.50890350341797},
                        {lat: 55.7868537902832, lng: 37.50885772705078},
                    ],
                ],
            },
            // GmapPolyline
            gmapPolylineOptions: {
                linePath: [
                    {
                        path: [
                            {lng: 38.23979568481445, lat: 55.57930374145508},
                            {lng: 38.2402229309082, lat: 55.57943344116211},
                            {lng: 38.240013122558594, lat: 55.579315185546875},
                            {lng: 38.239898681640625, lat: 55.5792121887207},
                            {lng: 38.23997497558594, lat: 55.57919692993164},
                            {lng: 38.24005889892578, lat: 55.579280853271484},
                            {lng: 38.240116119384766, lat: 55.57944869995117},
                            {lng: 38.240013122558594, lat: 55.57939529418945},
                            {lng: 38.240028381347656, lat: 55.579345703125},
                            {lng: 38.240028381347656, lat: 55.579345703125},
                            {lng: 38.24009704589844, lat: 55.57940673828125},
                            {lng: 38.24009704589844, lat: 55.57940673828125},
                            {lng: 38.238922119140625, lat: 55.57969284057617},
                            {lng: 38.238922119140625, lat: 55.57969284057617},
                            {lng: 38.23988342285156, lat: 55.57932662963867},
                            {lng: 38.23988342285156, lat: 55.57932662963867}
                        ]
                    },{
                        path: [
                            {lng: 37.5087776184082, lat: 55.78691482543945},
                            {lng: 37.50876998901367, lat: 55.78682327270508 },
                            {lng: 37.50890350341797, lat: 55.78738784790039},
                            {lng: 37.50885772705078, lat: 55.7868537902832},
                            {lng: 37.50872802734375, lat: 55.78681182861328},
                            {lng: 37.508819580078125, lat: 55.78688430786133}
                        ]
                    }
                ],
                editable: false,
                polylineOptions: {
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 4,
                }
            }
        },
        ready: function() {

        },
        methods: {
            updateEdited(mvcArray) {
                var paths = [];
                for (let i = 0; i < mvcArray.getLength(); i++) {
                    let path = [];
                    for (let j = 0; j < mvcArray.getAt(i).getLength(); j++) {
                        let point = mvcArray.getAt(i).getAt(j);
                        path.push({lat: point.lat(), lng: point.lng()});
                    }
                    paths.push(path);
                }
                this.edited = paths;
            }
        }
    });
    // })
}


function chart() {
    Vue.use(VueCharts);
    const app = new Vue({
        el: '#vue-chartLine',
        data: {
            audi:{
                speed: [7, 2, 2, 0, 0, 0, 0, 2, 0, 3, 7, 7, 6, 2, 7, 26, 37, 23, 10, 21, 33, 22, 0],
                labels: [
                    '00', '01', '03', '04', '05', '06', '07', '08', '09',
                    '10', '11', '12', '13', '14', '16', '17', '18', '19',
                    '20', '21', '22', '23'
                ],
                datalabel: '24113:83871_Infin_029 Speed',
                mywidth: 4,
                mypointbordercolor : '#f35009',
                hoverwidth : 1,
                hoverbackgroundcolor : '#636b6f',
                hoverbordercolor : '#ffd663',
            },

            bmwX6:{
                datalabel: '23483:BMW_X6_980 Speed',
                speed: [0, 5, 8, 6, 12, 6, 6, 10, 15, 13, 14, 20, 24, 12, 22, 49, 58, 57, 73, 73, 14, 18, 29],
                labels: [
                    '00', '01', '03', '04', '05', '06', '07', '08', '09',
                    '10', '11', '12', '13', '14', '16', '17', '18', '19',
                    '20', '21', '22', '23'
                ],
                mywidth: 4,
                mypointbordercolor : '#00c853',
                hoverwidth : 1,
                hoverbackgroundcolor : '#636b6f',
                hoverbordercolor : '#ffd663',
            },

            height: 100,
        },
    });
}

function addJavascript(jsname,pos) {
    var th = document.getElementsByTagName(pos)[0];
    var s = document.createElement('script');
    s.setAttribute('type','text/javascript');
    s.setAttribute('src',jsname);
    th.appendChild(s);
}